package fr.unicaen.certic.opentheso;

import fr.cnrs.opentheso.ws.ark.AbstractArkClient;
import fr.unicaen.certic.ark.ArkClient;
import fr.unicaen.certic.ark.ArkClientException;
import org.json.JSONObject;

import javax.json.Json;
import javax.json.JsonReader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UnicaenArkClient extends AbstractArkClient {

    private ArkClient arkClient;

    public UnicaenArkClient() {
        super();
    }

    public void setPropertiesArk(Properties propertiesArk) {
        this.propertiesArk = propertiesArk;
        this.arkClient = new ArkClient(
                Integer.parseInt(propertiesArk.getProperty("user")),
                propertiesArk.getProperty("password"),
                propertiesArk.getProperty("serverHost"));
    }

    @Override
    public boolean login() {
        try {
            //fake json - just fo fun ?
            JsonReader jsonReader = Json.createReader(new StringReader("{\"token\":\"1234\",\"user\":\"jdoe\"}"));
            loginJson = jsonReader.readObject();
            this.token = loginJson.getString("token").trim();
            return true;
        }
        catch(Exception e){
            Logger.getLogger(UnicaenArkClient.class.getName()).log(Level.SEVERE, e.getMessage());
            return false;
        }
    }

    @Override
    public boolean getArk(String ark) {
        try {
            Map<String,String> map = this.arkClient.read(ark);
            this.jsonArk = map.toString();//@todo : convert map to json string even in json seems to just be test as not null
        }
        catch(ArkClientException e){
            Logger.getLogger(UnicaenArkClient.class.getName()).log(Level.SEVERE, "GET ARK FAILED - " +e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public boolean addArk(String jsonString) {
        try {
            JSONObject json = new JSONObject(jsonString);
            String location = json.getString("urlTarget");
            HashMap<String, Object> metas = new HashMap<>();
            //add some metas
            if(json.getString("modificationDate") != null) {
                metas.put("modificationDate", json.getString("modificationDate"));
            }
            if(json.getString("type") != null) {
                metas.put("app", json.getString("type"));
            }
            if(json.getString("title") != null) {
                metas.put("title", json.getString("title"));
            }
            metas.put("type", "concept");
            //create with ark client
            this.idArk = this.arkClient.create(location, metas);
        }
        catch(ArkClientException e){
            return false;
        }
        return true;
    }

    @Override
    public boolean isArkExist(String ark) {
        return this.getArk(ark);
    }

    @Override
    public boolean updateArk(String jsonString) {
        JSONObject json = new JSONObject(jsonString);
        try {
            HashMap<String, Object> m = new HashMap<>();
            m.put("title", json.getString("title"));
            m.put("creator", json.getString("creator"));
            this.arkClient.update(json.getString("ark"), json.getString("urlTarget"), m);
        }
        catch(ArkClientException e){
            return false;
        }
        return true;
    }

    @Override
    public boolean updateUriArk(String jsonString) {
        return this.updateArk(jsonString);
    }

    @Override
    public boolean deleteHandle(String s){
        return true;
    }

    @Override
    public boolean isHandleExist(String idHandle) {
        return true;
    }

    @Override
    public String getIdHandle() {
        return "";
    }

}
