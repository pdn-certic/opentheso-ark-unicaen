# Client Ark Unicaen pour Opentheso

## Compilation

```
$mvn package
```

## Déploiement dans Opentheso

* Cloner, compiler et déployer l'Opentheso modifié permettant l'utilisation d'un client Ark custom :

https://github.com/choje/Opentheso2


* Copier le jar généré dans le répertoire *target* + le jar *lib/ark-[...]-jar-with-dependencies.jar* dans 
*/path/to/tomcat9/webapps/opentheso2/WEB-INF/lib/*
  
* Ajouter au fichier */path/to/tomcat9/webapps/opentheso2/WEB-INF/classes/preferences.properties*

````
arkClientClass=fr.unicaen.certic.opentheso.UnicaenArkClient
````

* le couple id/clé d'API ainsi que l'URL du serveur ARK sont à renseigner dans le formulaire
  *paramètres > identifiant > serveur ARK*. Renseigner *user ark* avec l'id de l'application, et *Passe Ark* avec la clé d'API.
  
  